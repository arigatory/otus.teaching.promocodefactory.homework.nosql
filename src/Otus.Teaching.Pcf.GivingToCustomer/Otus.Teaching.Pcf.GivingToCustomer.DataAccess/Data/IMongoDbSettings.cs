﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public interface IMongoDbSettings
    {
        string DatabaseName { get; set; }
        string ConnectionString { get; set; }
    }
}
