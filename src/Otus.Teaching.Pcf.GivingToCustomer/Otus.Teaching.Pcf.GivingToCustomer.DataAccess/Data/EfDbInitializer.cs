﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        IMongoDatabase database;
        public EfDbInitializer(IMongoDbSettings settings)
        {
            database = new MongoClient(settings.ConnectionString)
                .GetDatabase(settings.DatabaseName);
        }
        private protected string GetCollectionName(Type documentType)
        {
            return ((BsonCollectionAttribute)documentType.GetCustomAttributes(
                    typeof(BsonCollectionAttribute),
                    true)
                .FirstOrDefault())?.CollectionName;
        }


        public void InitializeDb()
        {
            var preferenceCollection = database
                  .GetCollection<Preference>(GetCollectionName(typeof(Preference)));
            var customerCollection = database
                  .GetCollection<Customer>(GetCollectionName(typeof(Customer)));


            preferenceCollection.DeleteMany(_ => true);
            preferenceCollection.InsertMany(FakeDataFactory.Preferences);

            customerCollection.DeleteMany(_ => true);
            customerCollection.InsertMany(FakeDataFactory.Customers);
        }
    }
}