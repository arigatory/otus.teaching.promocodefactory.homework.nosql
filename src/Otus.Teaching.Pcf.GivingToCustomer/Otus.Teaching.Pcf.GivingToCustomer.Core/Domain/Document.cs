﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public abstract class Document : IDocument
    {
        public Guid Id { get; set; }

    }
}
