﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("promocodes_customers")]
    public class PromoCodeCustomer : Document
    {
        public Guid PromoCodeId { get; set; }
        public virtual PromoCode PromoCode { get; set; }

        public Guid CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
